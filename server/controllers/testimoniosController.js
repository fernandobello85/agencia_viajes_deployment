const Testimonio = require('../models/Testimonios')

exports.mostrarTestimonios = async (req, res) => {
  const testimonios = await Testimonio.findAll()
  res.render('testimonios', {
    pagina: 'Testimonios',
    testimonios
  })
}

exports.agregarTestimonio = async (req, res) => {

  let{nombre, email, mensaje} = req.body
  let errores = []

  if(!nombre) {
    errores.push({'mensaje': 'Agrega tu nombre'})
  }
  if(!email) {
    errores.push({'mensaje': 'Agrega tu email'})
  }
  if(!mensaje) {
    errores.push({'mensaje': 'Agrega tu mensaje'})
  }

  if(errores.length > 0) {
    const testimonios = await Testimonio.findAll()
    res.render('testimonios', {
      errores,
      nombre,
      email,
      mensaje
    })
  } else {
    Testimonio.create({
      nombre,
      email,
      mensaje
    })
    .then(testimonio => res.redirect('/testimonios'))
    .catch(error => console.log(error))
  }
}

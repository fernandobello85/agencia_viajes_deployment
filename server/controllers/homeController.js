const Viaje = require('../models/Viajes')
const Testimonio = require('../models/Testimonios')

exports.consultasHomePage =  async (req, res) => {
  const promises = []
  const viajes = await Viaje.findAll({limit: 3})
  const testimonios = await Testimonio.findAll({limit: 3})

  res.render('index', {
    pagina: 'Proximos Viajes',
    clase: 'home',
    viajes,
    testimonios
  })
}
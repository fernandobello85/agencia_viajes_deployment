const express = require('express')
const router = express.Router()
const nosotrosController = require('../server/controllers/nosotrosController')
const homeController = require('../server/controllers/homeController')
const viajesController = require('../server/controllers/viajesController')
const testimoniosController = require('../server/controllers/testimoniosController')

module.exports = function() {
  router.get('/', homeController.consultasHomePage)
  router.get('/nosotros', nosotrosController.infoNosotros)
  router.get('/viajes', viajesController.mostrarViajes)
  router.get('/viaje/:id', viajesController.mostrarViaje)
  router.get('/testimonios', testimoniosController.mostrarTestimonios)
  router.post('/testimonios', testimoniosController.agregarTestimonio)

  return router
}